=====
AiiDA
=====
**Automated Interactive Infrastructure and Database for Computational Science**

AiiDA is a sophisticated framework designed from scratch to be a flexible
and scalable infrastructure for computational science. Being able to store
the full data provenance of each simulation, and based on a tailored
database solution built for efficient data mining implementations,
AiiDA gives the user the ability to interact seamlessly with any
number of HPC machines and codes thanks to its flexible plugin
interface, together with a powerful workflow engine for the automation 
of simulations.

The official homepage is at http://www.aiida.net.

How to cite AiiDA
-----------------
If you use AiiDA in your research, please consider citing the following work:

  Giovanni Pizzi, Andrea Cepellotti, Riccardo Sabatini, Nicola Marzari,
  and Boris Kozinsky, *AiiDA: Automated Interactive Infrastructure and Database 
  for Computational Science*, arXiv:1504.01163 (2015); http://www.aiida.net.

License
-------
The terms of the AiiDA license can be found in the LICENSE.txt file.
